﻿using Dungeon.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Views.Jobs
{
	public class JobsView : MonoBehaviour
	{
		[SerializeField] private GameObject arrowPrefab;

		private List<GameObject> arrows = new List<GameObject>();

		public void ShowPath(IReadOnlyList<JobsFieldView> path)
		{
			for (int i = 0; i < arrows.Count; i++)
				Destroy(arrows[i]);

			arrows.Clear();

			for(int i = 0; i < path.Count; i++)
			{
				var newArrow = PrefabLoader.InstantiatePrefab("Prefabs/Arrow", path[i].Position, Quaternion.identity);
				newArrow.GetComponent<Renderer>().material.color = path[i].IsAvailable ? Color.blue: Color.red;
				arrows.Add(newArrow);
			}
		}
	}
}