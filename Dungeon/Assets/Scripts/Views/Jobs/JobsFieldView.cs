﻿using UnityEngine;

namespace Dungeon.Views.Jobs
{
	public class JobsFieldView
    {
        private Vector3 position;
        private bool isAvailable;

        public Vector3 Position => position;
        public bool IsAvailable => isAvailable;

        public JobsFieldView(Vector3 position, bool isAvailable)
		{
            this.position = position;
            this.isAvailable = isAvailable;
		}
    }
}