using TMPro;
using UnityEngine;

namespace Dungeon.UI.Texts
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ParameterizedText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
		[SerializeField] private string defaultFormat;

		private void Reset()
		{
			text = GetComponent<TextMeshProUGUI>();
		}

		public void SetParameter(object parameter)
		{
			text.text = string.Format(defaultFormat, parameter);
		}
	}
}