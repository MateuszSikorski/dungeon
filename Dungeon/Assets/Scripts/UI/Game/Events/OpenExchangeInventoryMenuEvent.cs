namespace Dungeon.UI.Game.Events
{
	public class OpenExchangeInventoryMenuEvent
    {
        private Inventory.Inventory leftInventory;
        private Inventory.Inventory rightInventory;

        public Inventory.Inventory LeftInventory => leftInventory;
        public Inventory.Inventory RightInventory => rightInventory;

        public OpenExchangeInventoryMenuEvent(Inventory.Inventory leftInventory, Inventory.Inventory rightInventory)
		{
            this.leftInventory = leftInventory;
            this.rightInventory = rightInventory;
		}
    }
}