﻿using Dungeon.JobsSystem;
using Dungeon.JobsSystem.Jobs;
using System.Collections.Generic;

namespace Dungeon.UI.Game.Events
{
	public class OpenActionMenuEvent
	{
		private List<Job> availableActions;
		private IForJobGeneratorInfo jobGeneratorInfo;

		public List<Job> AvailableActions => availableActions;
		public IForJobGeneratorInfo JobGeneratorInfo => jobGeneratorInfo;

		public OpenActionMenuEvent(List<Job> availableActions, IForJobGeneratorInfo jobGeneratorInfo)
		{
			this.availableActions = availableActions;
			this.jobGeneratorInfo = jobGeneratorInfo;
		}
	}
}