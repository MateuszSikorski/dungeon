using Dungeon.Inventory;
using Dungeon.Utilities.Extentions;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI.Game.InventoryMenu
{
	public class InventoryItemButton : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Image itemIcon;
		[SerializeField] private GameObject countIndicator;
		[SerializeField] private TextMeshProUGUI count;

        private event Action<InventoryItem> onButtonClick;
		private InventoryItem item;

		private void Start()
		{
			button.SetListener(OnButtonClick);
		}

		public void Setup(InventoryItem item = null, Action<InventoryItem> onButtonClick = null)
		{
			this.item = item;
			this.onButtonClick = onButtonClick;

			var active = item != null;
			itemIcon.gameObject.SetActive(active);
			if (!active)
			{
				countIndicator.SetActive(false);
				return;
			}

			countIndicator.SetActive(item.Count > 1);
			count.text = active ? item.Count.ToString() : string.Empty;
		}

		private void OnButtonClick()
		{
			if(item != null)
				onButtonClick?.Invoke(item);
		}
	}
}