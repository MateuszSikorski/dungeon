using Dungeon.Inventory;
using Dungeon.UI.Texts;
using Dungeon.Utilities.Extentions;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI.Game.InventoryMenu
{
	public class ExchangeInventoryWindow : MonoBehaviour
	{
		[SerializeField] private Button closeButton;
		[SerializeField] private ParameterizedText weightText;
		[SerializeField] private List<InventoryItemButton> buttons;

		private Action onCloseButtonClick;
		private Action<InventoryItem> onItemButtonClick;
		private Inventory.Inventory inventory;

		private void OnDestroy()
		{
			inventory.onWeightChange -= SetWeightText;
		}

		public void Setup(Inventory.Inventory inventory, Action onCloseButtonClick, Action<InventoryItem> onItemButtonClick)
		{
			this.inventory = inventory;
			this.onCloseButtonClick = onCloseButtonClick;
			this.onItemButtonClick = onItemButtonClick;

			closeButton.SetListener(Close);
			SetButtons();
			SetWeightText(inventory.ItemsWeight);
			inventory.onWeightChange += SetWeightText;
		}

		public void Refresh()
		{
			SetButtons();
		}

		private void SetButtons()
		{
			for (int i = 0; i < buttons.Count; i++)
			{
				if (inventory.Items.Count > i)
					buttons[i].Setup(inventory.Items[i], OnClickItem);
				else
					buttons[i].Setup();
			}
		}

		private void OnClickItem(InventoryItem inventoryItem)
		{
			if (!inventory.Contains(inventoryItem.Data.ItemId))
				return;

			onItemButtonClick?.Invoke(inventoryItem);
		}

		private void Close()
		{
			onCloseButtonClick?.Invoke();
		}

		private void SetWeightText(float itemsWeight)
		{
			weightText.SetParameter(itemsWeight);
		}
	}
}