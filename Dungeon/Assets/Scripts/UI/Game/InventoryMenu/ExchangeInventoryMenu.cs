using Dungeon.Inventory;
using UnityEngine;

namespace Dungeon.UI.Game.InventoryMenu
{
	public class ExchangeInventoryMenu : MonoBehaviour
	{
		[SerializeField] private ExchangeInventoryWindow leftWindow;
		[SerializeField] private ExchangeInventoryWindow rightWindow;
		[SerializeField] private QuantitySelectionWindow quantitySelectionWindow;

		private Inventory.Inventory leftInventory;
		private Inventory.Inventory rightInventory;
		private Inventory.Inventory addInventory;
		private Inventory.Inventory removeInventory;

		public void Setup(Inventory.Inventory leftInventory, Inventory.Inventory rightInventory)
		{
			this.leftInventory = leftInventory;
			this.rightInventory = rightInventory;
			addInventory = null;
			removeInventory = null;

			leftWindow.Setup(leftInventory, Close, OnLeftWindowItemClick);
			rightWindow.Setup(rightInventory, Close, OnRightWindowItemClick);
			quantitySelectionWindow.gameObject.SetActive(false);
		}

		private void OnLeftWindowItemClick(InventoryItem inventoryItem)
		{
			addInventory = rightInventory;
			removeInventory = leftInventory;
			OpenQuantitySelectionWindow(inventoryItem);
		}

		private void OnRightWindowItemClick(InventoryItem inventoryItem)
		{
			addInventory = leftInventory;
			removeInventory = rightInventory;
			OpenQuantitySelectionWindow(inventoryItem);
		}

		private void RefreshWindows()
		{
			leftWindow.Refresh();
			rightWindow.Refresh();
		}

		private void OpenQuantitySelectionWindow(InventoryItem item)
		{
			if (item.Count == 1)
			{
				ExchangeItem(item);
				return;
			}

			quantitySelectionWindow.gameObject.SetActive(true);
			quantitySelectionWindow.Setup(item, ExchangeItem);
		}

		private void ExchangeItem(InventoryItem item)
		{
			quantitySelectionWindow.gameObject.SetActive(false);
			addInventory.AddItem(item.Data, item.Count);
			removeInventory.RemoveItem(item.Data.ItemId, item.Count);
			RefreshWindows();
		}

		private void Close()
		{
			Destroy(gameObject);
		}
	}
}