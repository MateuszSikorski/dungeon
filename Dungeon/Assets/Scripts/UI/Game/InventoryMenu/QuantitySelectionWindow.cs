using Dungeon.Inventory;
using Dungeon.UI.Texts;
using Dungeon.Utilities.Extentions;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI.Game.InventoryMenu
{
	public class QuantitySelectionWindow : MonoBehaviour
	{
		[SerializeField] private Image itemIcon;
		[SerializeField] private Button addButton;
		[SerializeField] private Button subButton;
		[SerializeField] private Button acceptButton;
		[SerializeField] private TextMeshProUGUI countText;
		[SerializeField] private ParameterizedText weightText;

		private int count;
		private InventoryItem item;
		private Action<InventoryItem> onAccept;

		public void Setup(InventoryItem item, Action<InventoryItem> onAccept)
		{
			this.item = item;
			this.onAccept = onAccept;
			count = 0;

			addButton.SetListener(Add);
			subButton.SetListener(Sub);
			acceptButton.SetListener(Accept);

			FormatCountText();
			FormatWeightText();
		}

		private void FormatCountText()
		{
			countText.text = $"{count}/{item.Count}";
		}

		private void FormatWeightText()
		{
			weightText.SetParameter(item.Data.Weight * count);
		}

		private void Add()
		{
			if(count < item.Count)
				count++;

			FormatCountText();
			FormatWeightText();
		}

		private void Sub()
		{
			if (count > 0)
				count--;

			FormatCountText();
			FormatWeightText();
		}

		private void Accept()
		{
			onAccept?.Invoke(new InventoryItem(count, item.Data));
		}
	}
}