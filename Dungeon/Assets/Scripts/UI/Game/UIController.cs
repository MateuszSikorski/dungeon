﻿using Dungeon.UI.Game.Events;
using Dungeon.UI.Game.InventoryMenu;
using Dungeon.Utilities;
using Dungeon.Utilities.EventBus;
using UnityEngine;

namespace Dungeon.UI.Game
{
	public class UIController : MonoBehaviour
	{
		private EventBus eventBus;

		private void Start()
		{
			eventBus = EventBus.Instance;
			eventBus.Subscribe<OpenActionMenuEvent>(OpenActionMenu);
			eventBus.Subscribe<OpenExchangeInventoryMenuEvent>(OpenExchangeInventoryMenu);
		}

		private void OnDestroy()
		{
			eventBus.Unsubscribe<OpenActionMenuEvent>(OpenActionMenu);
			eventBus.Unsubscribe<OpenExchangeInventoryMenuEvent>(OpenExchangeInventoryMenu);
		}

		private void OpenActionMenu(OpenActionMenuEvent eventData)
		{
			var menu = PrefabLoader.InstantiatePrefab<ActionMenu>("Prefabs/UI/ActionMenu/ActionMenu", transform);
			menu.Setup(eventData.AvailableActions, eventData.JobGeneratorInfo);
		}

		private void OpenExchangeInventoryMenu(OpenExchangeInventoryMenuEvent eventData)
		{
			var menu = PrefabLoader.InstantiatePrefab<ExchangeInventoryMenu>("Prefabs/UI/ExchangeInvetoryMenu/ExchangeInventoryMenu", transform);
			menu.Setup(eventData.LeftInventory, eventData.RightInventory);
		}
	}
}