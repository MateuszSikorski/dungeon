﻿using Dungeon.JobsSystem;
using Dungeon.JobsSystem.Jobs;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI.Game
{
	public class ActionButton : MonoBehaviour
	{
		public Action onButtonClick;

		[SerializeField] private Button button;
		[SerializeField] private TextMeshProUGUI text;

		private Job job;
		private IForJobGeneratorInfo jobGeneratorInfo;

		public void Setup(Job job, IForJobGeneratorInfo jobGeneratorInfo)
		{
			this.job = job;
			this.jobGeneratorInfo = jobGeneratorInfo;

			text.text = job.Type.ToString();
			button.onClick.AddListener(OnButtonClick);
		}

		private void OnButtonClick()
		{
			GameManager.Instance.Player.SetJobs(jobGeneratorInfo, job);

			onButtonClick?.Invoke();
		}
	}
}