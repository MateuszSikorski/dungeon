﻿using Dungeon.JobsSystem;
using Dungeon.JobsSystem.Jobs;
using Dungeon.Utilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Dungeon.UI.Game
{
	public class ActionMenu : MonoBehaviour
	{
		[SerializeField] private Button closeButton;
		[SerializeField] private Transform buttonList;

		public void Setup(List<Job> availableActions, IForJobGeneratorInfo jobGeneratorInfo)
		{
			closeButton.onClick.AddListener(Close);
			var playerCharacter = GameManager.Instance.Player.Character;

			for (int i = 0; i < availableActions.Count; i++)
			{
				if (!availableActions[i].CanBeUsed(playerCharacter))
					continue;

				var button = PrefabLoader.InstantiatePrefab<ActionButton>("Prefabs/UI/ActionMenu/ActionButton", buttonList);
				button.Setup(availableActions[i], jobGeneratorInfo);
				button.onButtonClick = Close;
			}
		}

		private void Close()
		{
			Destroy(gameObject);
		}
	}
}