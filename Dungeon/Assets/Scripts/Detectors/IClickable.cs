﻿using UnityEngine;

namespace Dungeon.Detectors
{
	public enum ClickedButton
	{
		Unknown,
		Action, //Left
		DefaultAction //Right
	}

	public interface IClickable
	{
		void Clicked(ClickedButton button, Vector3 worldClickPosition);
	}
}