﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Dungeon.Detectors
{
	[RequireComponent(typeof(Collider))]
	public class ClickDetector : MonoBehaviour, IPointerClickHandler
	{
		[SerializeField] private GameObject targetObject;

		private IClickable clickableObject;

		private void Start()
		{
			clickableObject = targetObject.GetComponent<IClickable>();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			clickableObject.Clicked(TranslateFromInputButton(eventData.button), eventData.pointerCurrentRaycast.worldPosition);
		}

		private ClickedButton TranslateFromInputButton(PointerEventData.InputButton button)
		{
			if (button == PointerEventData.InputButton.Left)
				return ClickedButton.Action;

			if (button == PointerEventData.InputButton.Right)
				return ClickedButton.DefaultAction;

			Debug.LogWarning($"Clicked unknown button from InputButton: {button}");
			return ClickedButton.Unknown;
		}
	}
}