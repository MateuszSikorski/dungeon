﻿using Dungeon;
using Dungeon.Entitys.EntityBases;
using Dungeon.JobsSystem.Jobs;
using Dungeon.Path;
using UnityEngine;

public class PathClickDetector : InteractiveEntity
{
	private PathField clickedField;

	public override MoveType MoveType => MoveType.Direct;
	public override PathField TargetField => clickedField;
	public override Vector3 PointToRotate => Vector3.forward;

	private void Start()
	{
		defaultJob = new MoveJob(MoveType.Direct);
	}

	protected override PathField GetField(GameManager gameManager, Vector3 worldClickPosition)
	{
		clickedField = gameManager.PathGrid.GetFieldByPosition(worldClickPosition);
		return clickedField;
	}
}
