using UnityEngine;

namespace Dungeon.Items
{
	public abstract class ItemData : ScriptableObject
	{
		[SerializeField] private int itemId;
		[SerializeField] private string itemNameId;
		[SerializeField] private float weight;

		public int ItemId => itemId;
		public string ItemNameId => itemNameId;
		public float Weight => weight;
		public abstract ItemType ItemType { get; }

		public void SetId(int newId)
		{
			itemId = newId;
		}
	}
}
