using Dungeon.Characters.Skills;
using UnityEngine;

namespace Dungeon.Items.ItemTypes
{
	[CreateAssetMenu(fileName = "NewTool", menuName = "Dungeon/ItemsData/NewTool", order = 2)]
	public class ToolData : ItemData
	{
		[SerializeField] private SkillName skill;
		[SerializeField] private int bonus;

		public override ItemType ItemType => ItemType.Tool;
		public SkillName Skill => skill;
		public int Bonus => bonus;
	}
}