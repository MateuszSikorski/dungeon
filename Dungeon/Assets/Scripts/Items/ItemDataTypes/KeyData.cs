using UnityEngine;

namespace Dungeon.Items.ItemTypes
{
	[CreateAssetMenu(fileName = "NewKey", menuName = "Dungeon/ItemsData/NewKey", order = 1)]
	public class KeyData : ItemData
	{
		public override ItemType ItemType => ItemType.Key;
	}
}