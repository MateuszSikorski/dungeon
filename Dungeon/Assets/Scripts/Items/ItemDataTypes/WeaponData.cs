using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Items.ItemTypes
{
	[CreateAssetMenu(fileName = "NewWeapon", menuName = "Dungeon/ItemsData/NewWeapon", order = 2)]
	public class WeaponData : ItemData
	{
		[SerializeField] private WeaponType weaponType;
		[SerializeField] private List<DamageType> damageTypes;

		public override ItemType ItemType => ItemType.Weapon;
		public WeaponType WeaponType => weaponType;
		public IReadOnlyList<DamageType> DamageTypes => damageTypes;
	}
}