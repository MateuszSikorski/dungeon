namespace Dungeon.Items
{
	public enum ItemType
	{
		Key,
		Weapon,
		Tool
	}

	public enum DamageType
	{
		Cut
	}

	public enum WeaponType
	{
		Sword,
		LongSword
	}
}