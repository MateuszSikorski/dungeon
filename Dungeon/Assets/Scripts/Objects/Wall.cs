﻿using Dungeon.Path;
using UnityEngine;

namespace Dungeon.Objects
{
	public class Wall : MonoBehaviour
	{
		public void InitPath(PathField field)
		{
			field?.SetAvailability(false);
		}
	}
}