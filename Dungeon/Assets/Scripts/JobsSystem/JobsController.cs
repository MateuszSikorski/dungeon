﻿using Dungeon.JobsSystem.Jobs;
using Dungeon.Path;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.JobsSystem
{
	public class JobsController
    {
        private List<Job> jobs;
		private PathFinder pathFinder;
		private Transform avatar;

		public List<Job> Jobs => jobs;

		public JobsController(Transform avatar)
		{
			jobs = new List<Job>();
			pathFinder = new PathFinder();
			this.avatar = avatar;
		}

		public void ClearJobs()
		{
			jobs.Clear();
		}

		public void CreateJobs(IForJobGeneratorInfo clickedEntity, Job job)
		{
			var playerField = GameManager.Instance.PathGrid.GetFieldByPosition(avatar.position);
			pathFinder.FindePath(playerField, clickedEntity.TargetField);

			if (job.Type == JobType.Move)
			{
				CreateMoveJobs(job as MoveJob);
				return;
			}

			CreateInteractionJobs(clickedEntity, job);
		}

		public void ValidateJobs(int actionPoints)
		{
			var availableActionPoints = actionPoints;
			for (int i = 0; i < jobs.Count; i++)
			{
				jobs[i].Validate(availableActionPoints);
				availableActionPoints -= jobs[i].Cost;
			}
		}

		public int AllJobsCost()
		{
			var cost = 0;
			for (int i = 0; i < jobs.Count; i++)
				cost += jobs[i].Cost;

			return cost;
		}

		private void CreateMoveJobs(MoveJob moveJob)
		{
			jobs.Clear();

			moveJob.SetPath(pathFinder.Path);
			jobs.Add(moveJob);
		}

		private void CreateInteractionJobs(IForJobGeneratorInfo info, Job job)
		{
			jobs.Clear();

			var avatarField = GameManager.Instance.PathGrid.GetFieldByPosition(avatar.position);
			if (info.TargetField == avatarField && info.MoveType == MoveType.Close)
				return;

			if (pathFinder.Path.Count <= 0 && info.TargetField != avatarField)
				return;

			jobs.Add(new MoveJob(pathFinder.Path, info.MoveType));
			jobs.Add(new RotateJob(info.PointToRotate));
			jobs.Add(job);
		}
	}
}