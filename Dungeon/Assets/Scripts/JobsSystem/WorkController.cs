﻿using Dungeon.JobsSystem.Jobs;
using Dungeon.JobsSystem.Workers;
using System.Collections.Generic;

namespace Dungeon.JobsSystem
{
	public class WorkController
	{
		private Dictionary<JobType, Worker> workers;
		private Queue<Job> jobs;
		private Worker currentWorker;

		public bool AllJobsDone => jobs.Count <= 0;

		public WorkController()
		{
			workers = new Dictionary<JobType, Worker>();
			jobs = new Queue<Job>();
			currentWorker = null;
		}

		public void InitWorker<T>(Worker worker) where T: Worker
		{
			if(workers.ContainsKey(worker.JobType))
			{
				workers[worker.JobType] = worker;
				return;
			}

			workers.Add(worker.JobType, worker);
		}

		public void Add(Job newJob)
		{
			jobs.Enqueue(newJob);
		}

		public void AddRange(List<Job> newJobs)
		{
			for (int i = 0; i < newJobs.Count; i++)
				jobs.Enqueue(newJobs[i]);
		}

		public void Work()
		{
			if (currentWorker != null)
			{
				currentWorker.Work();
				if (currentWorker.State == WorkState.InProgress)
					return;

				jobs.Dequeue();
				currentWorker = null;
			}

			if (AllJobsDone)
				return;

			var currentJob = jobs.Peek();
			currentWorker = workers[currentJob.Type];
			currentWorker.SetJob(currentJob);
			currentWorker.StartWork();
		}
	}
}