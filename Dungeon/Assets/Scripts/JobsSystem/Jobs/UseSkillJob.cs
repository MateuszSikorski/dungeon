﻿using Dungeon.Characters;
using Dungeon.Characters.Skills;
using Dungeon.Entitys;
using Dungeon.Items.ItemTypes;
using UnityEngine;

namespace Dungeon.JobsSystem.Jobs
{
	public class UseSkillJob : UseJobBase<IUseableSkillEntity>
	{
		private SkillName skillName;
		private int modifiers;

		public override JobType Type => JobType.UseSkill;

		public SkillName SkillName => skillName;
		public int Modifiers => modifiers;

		protected override int GetActionPoints => GameManager.Instance.GameplayData.UseSkillActionPointsCost;

		public UseSkillJob(SkillName skillName, int modifiers, IUseableSkillEntity entity)
		{
			this.skillName = skillName;
			this.modifiers = modifiers;
			this.entity = entity;
		}

		public override void Validate(int availableActionPoints)
		{
			isFeasible = Cost <= availableActionPoints;
		}

		public override bool CanBeUsed(CharacterModel characterModel)
		{
			var skillData = GameManager.Instance.GameplayData.GetSkillData(skillName);
			if (skillData.Type == SkillType.Special && !characterModel.Skills.ContainsKey(skillName))
				return false;

			ToolData toolData;
			if (skillData.NeedTool && !characterModel.Inventory.TryGetToolData(skillData.SkillName, out toolData))
			{
				Debug.Log($"Skill {skillData.SkillName} cannot be use without tool!");
				return false;
			}

			return true;
		}
	}
}