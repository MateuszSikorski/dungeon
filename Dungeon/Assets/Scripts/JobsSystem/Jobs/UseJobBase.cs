﻿namespace Dungeon.JobsSystem.Jobs
{
	public abstract class UseJobBase<T> : Job
	{
		protected T entity;

		public T Entity => entity;
		public override int Cost => isFeasible ? GetActionPoints : 0;

		protected abstract int GetActionPoints { get; }
	}
}