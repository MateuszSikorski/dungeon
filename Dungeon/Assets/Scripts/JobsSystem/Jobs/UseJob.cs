﻿using Dungeon.Entitys;

namespace Dungeon.JobsSystem.Jobs
{
	public class UseJob : UseJobBase<IUseableEntity>
	{
		public override JobType Type => JobType.Use;
		
		protected override int GetActionPoints => GameManager.Instance.GameplayData.UseActionPointsCost;

		public UseJob(IUseableEntity entity)
		{
			this.entity = entity;
		}

		public override void Validate(int availableActionPoints)
		{
			isFeasible = GameManager.Instance.GameplayData.UseActionPointsCost <= availableActionPoints;
		}
	}
}