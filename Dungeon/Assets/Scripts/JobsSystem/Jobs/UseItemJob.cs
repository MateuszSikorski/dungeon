using Dungeon.Characters;
using Dungeon.Entitys;

namespace Dungeon.JobsSystem.Jobs
{
	public class UseItemJob : UseJobBase<IUseableItemEntity>
	{
		public override JobType Type => JobType.UseItem;

		private bool removeItem;

		public bool RemoveItem => removeItem;

		protected override int GetActionPoints => GameManager.Instance.GameplayData.UseItemActionPointsCost;

		public UseItemJob(IUseableItemEntity entity, bool removeItem)
		{
			this.entity = entity;
			this.removeItem = removeItem;
		}

		public override void Validate(int availableActionPoints)
		{
			isFeasible = Cost <= availableActionPoints;
		}

		public override bool CanBeUsed(CharacterModel characterModel)
		{
			if (characterModel.Inventory.Contains(entity.ItemId()))
				return true;

			return false;
		}
	}
}