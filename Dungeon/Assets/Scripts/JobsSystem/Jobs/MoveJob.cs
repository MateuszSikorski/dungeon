﻿using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.JobsSystem.Jobs
{
	public enum MoveType
	{
		Direct,
		Close
	}

	public class MoveJob : Job
	{
		private List<Vector3> path;
		private MoveType moveType;
		public List<Vector3> Path => path;
		public override JobType Type => JobType.Move;

		public override int Cost => path.Count * GameManager.Instance.GameplayData.MoveActionPointsCost;

		public MoveJob(MoveType moveType)
		{
			this.moveType = moveType;
		}

		public MoveJob(List<Vector3> path, MoveType moveType)
		{
			this.moveType = moveType;
			SetPath(path);
		}

		public void SetPath(List<Vector3> path)
		{
			if (moveType == MoveType.Close && path.Count > 0)
				path.RemoveAt(path.Count - 1);

			this.path = path;
		}

		public override void Validate(int availableActionPoints)
		{
			if(availableActionPoints <= 0)
			{
				isFeasible = false;
				return;
			}

			var pointsPerField = GameManager.Instance.GameplayData.MoveActionPointsCost;
			var fields = 0;
			var usedPoints = 0;
			for(int i = 0; i < path.Count; i++)
			{
				if (usedPoints > availableActionPoints - pointsPerField)
					break;

				usedPoints += pointsPerField;
				fields++;
			}

			path.RemoveRange(fields, path.Count - fields);
		}
	}
}