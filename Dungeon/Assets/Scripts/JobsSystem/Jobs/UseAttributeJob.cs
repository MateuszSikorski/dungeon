﻿using Dungeon.Characters.Attributes;
using Dungeon.Entitys;

namespace Dungeon.JobsSystem.Jobs
{
	public class UseAttributeJob : UseJobBase<IUseableAttributeEntity>
	{
		private AttributeType attributeType;
		private int modifiers;

		public override JobType Type => JobType.UseAttribute;

		public AttributeType AttributeType => attributeType;
		public int Modifiers => modifiers;

		protected override int GetActionPoints => GameManager.Instance.GameplayData.UseAttributeActionPointsCost;

		public UseAttributeJob(AttributeType attributeType, int modifiers, IUseableAttributeEntity entity)
		{
			this.attributeType = attributeType;
			this.modifiers = modifiers;
			this.entity = entity;
		}

		public override void Validate(int availableActionPoints)
		{
			isFeasible = Cost <= availableActionPoints;
		}
	}
}