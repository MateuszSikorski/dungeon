﻿using Dungeon.Characters;

namespace Dungeon.JobsSystem.Jobs
{
	public enum JobType
	{
		Move,
		Rotate,
		Use,
		UseAttribute,
		UseSkill,
		UseItem
	}

	public abstract class Job
	{
		protected bool isFeasible = true;

		public bool IsFeasible => isFeasible;

		public abstract JobType Type { get; }
		public abstract int Cost { get; }
		public abstract void Validate(int availableActionPoints);

		public virtual bool CanBeUsed(CharacterModel characterModel)
		{
			return true;
		}
	}
}