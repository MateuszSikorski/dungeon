﻿using UnityEngine;

namespace Dungeon.JobsSystem.Jobs
{
	public class RotateJob : Job
	{
		private Vector3 pointToRotate;

		public Vector3 PointToRotate => pointToRotate;
		public override JobType Type => JobType.Rotate;

		public override int Cost => isFeasible ? GameManager.Instance.GameplayData.RotateActionPointsCost : 0;

		public RotateJob(Vector3 pointToRotate)
		{
			this.pointToRotate = pointToRotate;
		}

		public override void Validate(int availableActionPoints)
		{
			isFeasible = GameManager.Instance.GameplayData.RotateActionPointsCost <= availableActionPoints;
		}
	}
}