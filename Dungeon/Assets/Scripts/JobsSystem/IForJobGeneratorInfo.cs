﻿using Dungeon.JobsSystem.Jobs;
using Dungeon.Path;
using UnityEngine;

namespace Dungeon.JobsSystem
{
	public interface IForJobGeneratorInfo
	{
		MoveType MoveType { get; }
		PathField TargetField { get; }
		Vector3 PointToRotate { get; }
	}
}