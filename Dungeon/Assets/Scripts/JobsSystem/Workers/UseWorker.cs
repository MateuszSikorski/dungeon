﻿using Dungeon.Characters;
using Dungeon.JobsSystem.Jobs;

namespace Dungeon.JobsSystem.Workers
{
	public class UseWorker : UseCharacterWorkerBase<UseJob>
	{
		public override JobType JobType => JobType.Use;

		public UseWorker(CharacterModel character) : base(character)
		{
		}

		protected override void ProcessCalculation()
		{
		}

		protected override void ProcessJob()
		{
			job.Entity.Use(character);
			finnished = true;
		}
	}
}