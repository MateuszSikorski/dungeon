﻿using Dungeon.JobsSystem.Jobs;
using UnityEngine;

namespace Dungeon.JobsSystem.Workers
{
	public class RotateWorker : Worker<RotateJob>
	{
		private Transform avatar;
		private float rotateOffset;
		private float speed;
		private Vector3 direction;

		public override JobType JobType => JobType.Rotate;

		public RotateWorker(Transform avatar, float rotateOffset, float speed)
		{
			this.avatar = avatar;
			this.rotateOffset = rotateOffset;
			this.speed = speed;
		}

		protected override bool IsJobFinnished()
		{
			if (1f - Vector3.Dot(avatar.forward, direction) <= rotateOffset)
				return true;

			return false;
		}

		protected override void PreparationToWork()
		{
			direction = job.PointToRotate - avatar.position;
			direction.y = 0f;
			direction.Normalize();
		}

		protected override void ProcessJob()
		{
			var newDirection = Vector3.RotateTowards(avatar.forward, direction, speed * 5f * Time.deltaTime, 0f);
			newDirection.y = 0f;
			avatar.rotation = Quaternion.LookRotation(newDirection);

			if (IsJobFinnished())
				avatar.rotation = Quaternion.LookRotation(direction);
		}
	}
}