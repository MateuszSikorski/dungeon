﻿using Dungeon.Characters;
using Dungeon.JobsSystem.Jobs;

namespace Dungeon.JobsSystem.Workers
{
	public abstract class UseCharacterWorkerBase<T> : Worker<T> where T: Job
	{
		protected CharacterModel character;
		protected bool finnished = false;

		public UseCharacterWorkerBase(CharacterModel character)
		{
			this.character = character;
		}

		protected override bool IsJobFinnished()
		{
			return finnished;
		}

		protected override void PreparationToWork()
		{
			finnished = false;
		}

		protected override void ProcessJob()
		{
			ProcessCalculation();
			finnished = true;
		}

		protected abstract void ProcessCalculation();
	}
}