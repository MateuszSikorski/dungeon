﻿using Dungeon.JobsSystem.Jobs;
using System.Linq;
using UnityEngine;

namespace Dungeon.JobsSystem.Workers
{
	public class MoveWorker : Worker<MoveJob>
	{
		private Transform avatar;
		private float positionOffset;
		private float speed;
		private int pathIndex;

		public override JobType JobType => JobType.Move;

		public MoveWorker(Transform avatar, float positionOffset, float speed)
		{
			this.avatar = avatar;
			this.positionOffset = positionOffset;
			this.speed = speed;
		}

		protected override bool IsJobFinnished()
		{
			if (pathIndex >= job.Path.Count)
				return true;

			return false;
		}

		protected override void ProcessJob()
		{
			var currentTarget = job.Path[pathIndex];

			var direction = currentTarget - avatar.position;
			direction.y = 0f;
			if (direction.magnitude < positionOffset)
			{
				if (pathIndex >= job.Path.Count - 1)
					avatar.position = currentTarget;

				pathIndex++;
			}
			else
			{
				var newDirection = Vector3.RotateTowards(avatar.forward, direction, speed * 5f * Time.deltaTime, 0f);
				newDirection.y = 0f;
				avatar.rotation = Quaternion.LookRotation(newDirection);
				avatar.position += newDirection * speed * Time.deltaTime;
			}
		}

		protected override void PreparationToWork()
		{
			if(job == null)
			{
				state = WorkState.Done;
				return;
			}

			pathIndex = 0;
		}
	}
}