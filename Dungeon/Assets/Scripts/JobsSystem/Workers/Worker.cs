﻿using Dungeon.JobsSystem.Jobs;

namespace Dungeon.JobsSystem.Workers
{
	public enum WorkState
	{
		InProgress,
		Done
	}

	public abstract class Worker
	{
		protected WorkState state = WorkState.Done;

		public WorkState State => state;

		public abstract JobType JobType { get; }

		protected abstract bool IsFeasible { get; }

		public abstract void SetJob(Job newJob);

		public void StartWork()
		{
			if (!IsFeasible)
				return;

			if (state == WorkState.Done)
			{
				PreparationToWork();
				state = WorkState.InProgress;
			}
		}

		public void Work()
		{
			if (IsJobFinnished())
				state = WorkState.Done;

			if (state == WorkState.InProgress)
				ProcessJob();
		}

		protected abstract bool IsJobFinnished();
		protected abstract void ProcessJob();
		protected abstract void PreparationToWork();
	}

	public abstract class Worker<T> : Worker where T: Job
	{
		protected T job;

		protected override bool IsFeasible => job.IsFeasible;

		public override void SetJob(Job newJob)
		{
			if (newJob is T)
				job = newJob as T;
		}
	}
}