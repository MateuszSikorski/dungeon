﻿using Dungeon.Characters;
using Dungeon.Characters.Skills;
using Dungeon.Items.ItemTypes;
using Dungeon.JobsSystem.Jobs;
using UnityEngine;

namespace Dungeon.JobsSystem.Workers
{
	public class UseSkillWorker : UseCharacterWorkerBase<UseSkillJob>
	{
		public override JobType JobType => JobType.UseSkill;

		public UseSkillWorker(CharacterModel character) : base(character)
		{
		}

		protected override void PreparationToWork()
		{
			base.PreparationToWork();

			var skillData = GameManager.Instance.GameplayData.GetSkillData(job.SkillName);
			if (!character.Skills.ContainsKey(job.SkillName) && skillData.Type == SkillType.Common)
			{
				character.InitSkill(job.SkillName, new Skill(0, 0, false, skillData));
				Debug.Log($"Add new untrain skill: {job.SkillName}");
			}
		}

		protected override void ProcessCalculation()
		{
			Debug.Log($"Use skill: {job.SkillName}");
			if(!character.Skills.ContainsKey(job.SkillName))
			{
				Debug.Log($"Trying to use unowned skill: {job.SkillName}");
				finnished = true;
				return;
			}

			var skill = character.Skills[job.SkillName];
			var attribute = character.Attributes[skill.AttributeType];
			var difficultyLevel = attribute.Value + skill.Value + job.Modifiers;

			ToolData toolData;
			if (character.Inventory.TryGetToolData(job.SkillName, out toolData))
				difficultyLevel += toolData.Bonus;

			var result = DiceRoller.Roll(difficultyLevel);

			if(result.Item1 == RollResultType.CriticalFailure && toolData != null)
			{
				Debug.Log($"Durring {job.SkillName} you broke tool with ID: {toolData.ItemId}");
				character.Inventory.RemoveItem(toolData.ItemId);
			}

			job.Entity.SkillTestResult(job.SkillName, result.Item1, result.Item2);

			if (result.Item1 == RollResultType.CriticalSuccess)
				skill.AddTrainingPoints(GameManager.Instance.GameplayData.TrainingPoinstForCriticalSuccess);
			if (result.Item1 == RollResultType.Success)
				skill.AddTrainingPoints(GameManager.Instance.GameplayData.TrainingPointsForSuccess);
		}
	}
}