using Dungeon.Characters;
using Dungeon.JobsSystem.Jobs;
using UnityEngine;

namespace Dungeon.JobsSystem.Workers
{
	public class UseItemWorker : UseCharacterWorkerBase<UseItemJob>
	{
		public override JobType JobType => JobType.UseItem;

		public UseItemWorker(CharacterModel character) : base(character)
		{
		}

		protected override void ProcessCalculation()
		{
			Debug.Log($"Use item: {job.Entity.ItemId()}");

			if (!character.Inventory.Contains(job.Entity.ItemId()))
			{
				Debug.Log($"Trying to use unowned item: {job.Entity.ItemId()}");
				return;
			}

			job.Entity.UseItem();
			if (job.RemoveItem)
				character.Inventory.RemoveItem(job.Entity.ItemId());
		}
	}
}