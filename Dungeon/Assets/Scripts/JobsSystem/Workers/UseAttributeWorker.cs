﻿using Dungeon.Characters;
using Dungeon.JobsSystem.Jobs;
using UnityEngine;

namespace Dungeon.JobsSystem.Workers
{
	public class UseAttributeWorker : UseCharacterWorkerBase<UseAttributeJob>
	{
		public override JobType JobType => JobType.UseAttribute;

		public UseAttributeWorker(CharacterModel character) : base(character)
		{
		}

		protected override void ProcessCalculation()
		{
			Debug.Log($"Use attribute: {job.AttributeType}");
			var attribute = character.Attributes[job.AttributeType];
			var result = DiceRoller.Roll(attribute.Value + job.Modifiers);
			job.Entity.AttributeTestResult(job.AttributeType, result.Item1, result.Item2);

			if (result.Item1 == RollResultType.CriticalSuccess)
				attribute.AddTrainingPoints(GameManager.Instance.GameplayData.TrainingPoinstForCriticalSuccess);
			if (result.Item1 == RollResultType.Success)
				attribute.AddTrainingPoints(GameManager.Instance.GameplayData.TrainingPointsForSuccess);
		}
	}
}