using Dungeon.Characters.Skills;
using Dungeon.Items;
using Dungeon.Items.ItemTypes;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Inventory
{
	public class Inventory
	{
		public event Action<float> onWeightChange;

		private List<InventoryItem> items = new List<InventoryItem>();
		private float itemsWeight = 0f;

		public IReadOnlyList<InventoryItem> Items => items;
		public float ItemsWeight => itemsWeight;

		public void AddItem(ItemData itemData, int count = 1)
		{
			if (count <= 0)
				return;

			var item = items.Find(i => i.Data.ItemId == itemData.ItemId);
			if (item != null)
				item.Add(count);
			else
				items.Add(new InventoryItem(count, itemData));

			itemsWeight += itemData.Weight * count;
			onWeightChange?.Invoke(itemsWeight);

			Debug.Log($"Add {count} item/s with id: {itemData.ItemId}");
		}

		public void RemoveItem(int itemId, int count = 1)
		{
			if (count <= 0)
				return;

			var item = items.Find(i => i.Data.ItemId == itemId);
			if (item == null)
				return;

			var itemCount = item.Count;
			if (count > itemCount)
				count = itemCount;

			item.Remove(count);
			if (item.Count <= 0)
				items.Remove(item);

			itemsWeight -= item.Data.Weight * count;
			onWeightChange?.Invoke(itemsWeight);

			Debug.Log($"Remove {count} item/s with id: {itemId}");
		}

		public bool Contains(int itemId, int count = 1)
		{
			var item = items.Find(i => i.Data.ItemId == itemId);
			if (item == null)
				return false;

			return item.Count >= count ? true : false;
		}

		public bool TryGetToolData(SkillName skill, out ToolData toolData)
		{
			toolData = null;
			var tool = items.Find(i =>
			{
				if (i.Data is ToolData)
				{
					var item = i.Data as ToolData;
					if (item.Skill == skill)
						return true;
				}

				return false;
			});

			if (tool == null)
				return false;

			toolData = tool.Data as ToolData;
			return true;
		}
	}
}