using Dungeon.Items;
using System;
using UnityEngine;

namespace Dungeon.Inventory
{
    [Serializable]
	public class InventoryItem
    {
        [SerializeField] private int count = 1;
        [SerializeField] private ItemData data;

        public ItemData Data => data;
        public int Count => count;

        public InventoryItem(int count, ItemData data)
		{
            this.count = count;
            this.data = data;
		}

        public void Add(int count = 1)
		{
            this.count += count;
		}

        public void Remove(int count = 1)
		{
            this.count -= count;
		}
    }
}