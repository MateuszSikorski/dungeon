﻿using Dungeon.Characters.Attributes;
using Dungeon.Characters.Skills;
using System.Collections.Generic;

namespace Dungeon.Characters
{
	public class CharacterModel
	{
		private Dictionary<AttributeType, Attribute> attributes;
		private Dictionary<SkillName, Skill> skills;
		private Inventory.Inventory inventory;

		public IReadOnlyDictionary<AttributeType, Attribute> Attributes => attributes;
		public IReadOnlyDictionary<SkillName, Skill> Skills => skills;
		public Inventory.Inventory Inventory => inventory;

		public CharacterModel()
		{
			attributes = new Dictionary<AttributeType, Attribute>();
			skills = new Dictionary<SkillName, Skill>();
			inventory = new Inventory.Inventory();
		}

		public void InitAttribute(AttributeType type, Attribute attribute)
		{
			attributes[type] = attribute;
		}

		public void InitSkill(SkillName name, Skill skill)
		{
			skills[name] = skill;
		}
	}
}