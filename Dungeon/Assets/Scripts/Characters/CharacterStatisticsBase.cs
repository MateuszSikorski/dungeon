﻿using Dungeon.Settings;

namespace Dungeon.Characters
{
	public abstract class CharacterStatisticsBase
	{
		protected int baseValue;
		protected int trainingPoints;
		protected GameplayData gameplayData;

		public abstract int Value { get; }
		public int TrainingPoints => trainingPoints;

		protected abstract int MaxTrainingPoints { get; }
		protected abstract int MaxLevel { get; }

		public CharacterStatisticsBase(int baseValue, int trainingPoints)
		{
			gameplayData = GameManager.Instance.GameplayData;

			this.baseValue = baseValue;
			this.trainingPoints = trainingPoints;
		}

		public void AddTrainingPoints(int value)
		{
			if (baseValue >= MaxLevel)
				return;

			trainingPoints += value;
			var maxTrainingPoints = MaxTrainingPoints;
			if (trainingPoints < maxTrainingPoints)
				return;

			trainingPoints -= maxTrainingPoints;
			LevelUp();
			AfterLevelUp();
		}

		protected virtual void AfterLevelUp()
		{
		}

		protected virtual void LevelUp()
		{
			baseValue++;
		}
	}
}