﻿namespace Dungeon.Characters.Attributes
{
	public enum AttributeType
	{
		Strength,
		Dexterity
	}
}