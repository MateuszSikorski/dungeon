﻿using Dungeon.Characters.Skills;
using Dungeon.Utilities.EventBus;

namespace Dungeon.Characters.Attributes
{
	public class Attribute : CharacterStatisticsBase
	{
		private AttributeType attributeType;

		public override int Value => baseValue;

		protected override int MaxTrainingPoints => gameplayData.AttributeTrainingPointsForLevel;
		protected override int MaxLevel => gameplayData.MaxAttributeLevel;

		public Attribute(int baseValue, int trainingPoints, AttributeType attributeType) : base(baseValue, trainingPoints)
		{
			this.attributeType = attributeType;
			EventBus.Instance.Subscribe<SkillLevelUpEvent>(OnSkillLevelUpEvent);
		}

		~Attribute()
		{
			EventBus.Instance.Unsubscribe<SkillLevelUpEvent>(OnSkillLevelUpEvent);
		}

		private void OnSkillLevelUpEvent(SkillLevelUpEvent eventData)
		{
			if(eventData.AttributeType == attributeType)
				AddTrainingPoints(gameplayData.AttributeTrainingPointsForSkillLevelUp);
		}
	}
}