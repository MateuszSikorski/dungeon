﻿using Dungeon.Characters.Attributes;

namespace Dungeon.Characters.Skills
{
	public class SkillLevelUpEvent
	{
		private SkillName skillName;
		private AttributeType attributeType;

		public SkillName SkillName => skillName;
		public AttributeType AttributeType => attributeType;

		public SkillLevelUpEvent(SkillName skillName, AttributeType attributeType)
		{
			this.skillName = skillName;
			this.attributeType = attributeType;
		}
	}
}