﻿using Dungeon.Characters.Attributes;
using Dungeon.Utilities.EventBus;

namespace Dungeon.Characters.Skills
{
	public class Skill : CharacterStatisticsBase
	{
		private SkillData data;
		private bool isTrained;

		public AttributeType AttributeType => data.AttributeType;
		public bool IsTrained => isTrained;
		public SkillType Type => data.Type;
		public override int Value => GetValue();

		protected override int MaxTrainingPoints => gameplayData.SkillTrainingPointsForLevel;
		protected override int MaxLevel => gameplayData.MaxSkillLevel;

		public Skill(int baseValue, int trainingPoints, bool isTrained, SkillData data) : base(baseValue, trainingPoints)
		{
			this.data = data;
			this.isTrained = isTrained;
		}

		protected override void AfterLevelUp()
		{
			EventBus.Instance.Fire(new SkillLevelUpEvent(data.SkillName, data.AttributeType));
		}

		protected override void LevelUp()
		{
			if(!isTrained)
			{
				isTrained = true;
				baseValue = 0;

				return;
			}

			base.LevelUp();
		}

		private int GetValue()
		{
			if (!isTrained && data.Type == SkillType.Special)
				return -1000;

			if (isTrained)
				return baseValue;

			return gameplayData.UntrainedSkillModifier;
		}
	}
}