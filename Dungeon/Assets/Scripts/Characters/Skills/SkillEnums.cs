﻿namespace Dungeon.Characters.Skills
{
	public enum SkillType
	{
		Common,
		Special
	}

	public enum SkillName
	{
		Lockpicking
	}
}