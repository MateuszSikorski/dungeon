﻿using Dungeon.Characters.Attributes;
using UnityEngine;

namespace Dungeon.Characters.Skills
{
	[CreateAssetMenu(fileName = "NewSkill", menuName = "Dungeon/Character/Skill", order = 1)]
	public class SkillData : ScriptableObject
	{
		[SerializeField] private SkillName skillName;
		[SerializeField] private AttributeType attributeType;
		[SerializeField] private SkillType type;
		[SerializeField] private bool needTool;

		public SkillName SkillName => skillName;
		public AttributeType AttributeType => attributeType;
		public SkillType Type => type;
		public bool NeedTool => needTool;
	}
}