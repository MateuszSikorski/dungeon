﻿using Dungeon;
using Dungeon.Characters;
using Dungeon.Characters.Attributes;
using Dungeon.Characters.Skills;
using Dungeon.Items;
using Dungeon.JobsSystem;
using Dungeon.JobsSystem.Jobs;
using Dungeon.JobsSystem.Workers;
using Dungeon.Path;
using Dungeon.Views.Jobs;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	[SerializeField] private float positionOffset = 0.1f;
	[SerializeField] private float speed;
	[SerializeField] private int actionPoints;

	private WorkController workController;
	private JobsController jobsController;
	private IForJobGeneratorInfo currentClickedEntity;
	private PathField currentClickedEntityField;
	private CharacterModel character;

	public CharacterModel Character => character;

	private void Awake()
	{
		character = new CharacterModel();

		character.InitAttribute(AttributeType.Strength, new Attribute(8, 0, AttributeType.Strength));
		character.InitAttribute(AttributeType.Dexterity, new Attribute(7, 0, AttributeType.Dexterity));

		character.InitSkill(SkillName.Lockpicking, new Skill(1, 0, true, GameManager.Instance.GameplayData.GetSkillData(SkillName.Lockpicking)));

		workController = new WorkController();
		workController.InitWorker<MoveWorker>(new MoveWorker(transform, positionOffset, speed));
		workController.InitWorker<RotateWorker>(new RotateWorker(transform, 0.1f, speed));
		workController.InitWorker<UseWorker>(new UseWorker(character));
		workController.InitWorker<UseAttributeWorker>(new UseAttributeWorker(character));
		workController.InitWorker<UseSkillWorker>(new UseSkillWorker(character));
		workController.InitWorker<UseItemWorker>(new UseItemWorker(character));

		jobsController = new JobsController(transform);
	}

	private void Update()
	{
		workController.Work();
	}

	public void SetJobs(IForJobGeneratorInfo clickedEntity, Job job)
	{
		jobsController.CreateJobs(clickedEntity, job);
		StartJobs();
		ShowPath(jobsController.Jobs);
	}

	public void SetClickedEntity(IForJobGeneratorInfo clickedEntity, Job job, PathField clickedEntityField)
	{
		if (!workController.AllJobsDone)
			return;

		if (currentClickedEntity != clickedEntity)
		{
			currentClickedEntity = clickedEntity;
			currentClickedEntityField = clickedEntityField;
			jobsController.CreateJobs(clickedEntity, job);
			ShowPath(jobsController.Jobs);
		}
		else
		{
			if (currentClickedEntityField != clickedEntityField)
			{
				currentClickedEntityField = clickedEntityField;
				jobsController.CreateJobs(clickedEntity, job);
				ShowPath(jobsController.Jobs);
			}
			else
			{
				StartJobs();
				currentClickedEntity = null;
			}
		}
	}

	private void StartJobs()
	{
		jobsController.ValidateJobs(actionPoints);
		workController.AddRange(jobsController.Jobs);
		jobsController.ClearJobs();
	}

	private void ShowPath(List<Job> jobs)
	{
		for (int i = 0; i < jobs.Count; i++)
		{
			if (jobs[i].Type != JobType.Move)
				continue;

			var showJobs = new List<JobsFieldView>();
			var moveJob = jobs[i] as MoveJob;
			var usedPoints = 0;
			for (int j = 0; j < moveJob.Path.Count; j++)
			{
				showJobs.Add(new JobsFieldView(moveJob.Path[j], usedPoints < actionPoints));
				usedPoints++;
			}

			GameManager.Instance.JobsView.ShowPath(showJobs);
		}
	}
}
