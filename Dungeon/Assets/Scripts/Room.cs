﻿using Dungeon.Magic.Sources;
using Dungeon.Objects;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon
{
	public class Room : MonoBehaviour
	{
		[SerializeField] private List<MagicSource> magicSources = new List<MagicSource>();

		private void Awake()
		{
			magicSources.AddRange(GetComponentsInChildren<MagicSource>());
		}

		private void Start()
		{
			SetPath();
		}
		private void SetPath()
		{
			var walls = GetComponentsInChildren<Wall>();
			var pathGrid = GameManager.Instance.PathGrid;
			foreach (var wall in walls)
				wall.InitPath(pathGrid.GetFieldByPosition(wall.transform.position));
		}
	}
}