﻿using Dungeon.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Path
{
	public class PathGrid : MonoBehaviour
    {
		[SerializeField] private int xSize;
		[SerializeField] private int ySize;

		private List2D<PathField> grid;

		private void Awake()
		{
			GenerateGrid();
		}

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.white;
			Gizmos.DrawWireCube(transform.position, new Vector3(xSize, 0.2f, ySize));

			if (grid == null)
				return;

			var size = Vector3.one * 0.3f;
			for (int x = 0; x < xSize; x++)
			{
				for (int y = 0; y < ySize; y++)
				{
					var field = grid[x, y];
					Gizmos.color = Color.red;
					if (field.IsAvailable)
						Gizmos.color = Color.yellow;
					if (field.IsBlock)
						Gizmos.color = Color.green;

					Gizmos.DrawCube(field.Position, size);
				}
			}
		}

		public PathField GetFieldByPosition(Vector3 position)
		{
			for(int x = 0; x < grid.SizeX; x++)
			{
				for(int y = 0; y < grid.SizeY; y++)
				{
					var field = grid[x, y];

					if (field.Position.x + 0.5f <= position.x || field.Position.x - 0.5f > position.x)
						continue;
					if (field.Position.z + 0.5f <= position.z || field.Position.z - 0.5f > position.z)
						continue;

					return field;
				}
			}

			return null;
		}

		public List<PathField> GetNeighbours(PathField field)
		{
			var neighbours = new List<PathField>();

			for(int x = -1; x <= 1; x++)
			{
				for (int y = -1; y <= 1; y++)
				{
					if (x == 0 && y == 0)
						continue;

					var checkX = field.GridX + x;
					var checkY = field.GridY + y;

					if (checkX < 0 || checkX >= xSize || checkY < 0 || checkY >= ySize)
						continue;

					neighbours.Add(grid[checkX, checkY]);
				}
			}

			return neighbours;
		}

		private void GenerateGrid()
		{
			grid = new List2D<PathField>(xSize, ySize);

			var xOffset = xSize / 2 - 0.5f * (1 - (xSize % 2));
			var yOffset = ySize / 2 - 0.5f * (1 - (ySize % 2));

			for (int x = 0; x < xSize; x++)
			{
				for (int y = 0; y < ySize; y++)
					grid[x, y] = new PathField(new Vector3(transform.position.x - xOffset + x, transform.position.y, transform.position.z - yOffset + y), x, y);
			}
		}
	}
}