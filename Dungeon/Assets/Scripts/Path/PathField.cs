﻿using UnityEngine;

namespace Dungeon.Path
{
	public class PathField
	{
		public int gCost;
		public int hCost;
		public PathField parent;

		private Vector3 position;
		private bool isAvailable = true;
		private bool isBlock;

		private int gridX;
		private int gridY;

		public Vector3 Position => position;
		public bool IsAvailable => isAvailable;
		public bool IsBlock => isBlock;

		public int GridX => gridX;
		public int GridY => gridY;

		public int FCost => gCost + hCost;

		public PathField(Vector3 position, int x, int y)
		{
			this.position = new Vector3(position.x, position.y, position.z);
			gridX = x;
			gridY = y;
		}

		public void SetAvailability(bool value)
		{
			isAvailable = value;
		}

		public void SetIsBlock(bool value)
		{
			isBlock = value;
		}
	}
}