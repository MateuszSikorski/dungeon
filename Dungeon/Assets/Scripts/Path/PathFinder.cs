﻿using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Path
{
	public class PathFinder
	{
		private List<Vector3> path = new List<Vector3>();

		public List<Vector3> Path => path;

		public void FindePath(PathField startField, PathField targetField)
		{
			path.Clear();
			targetField.parent = null;
			var grid = GameManager.Instance.PathGrid;
			var openSet = new List<PathField>();
			var closeSet = new List<PathField>();
			openSet.Add(startField);

			while(openSet.Count > 0)
			{
				var currentField = openSet[0];
				for(int i = 1; i < openSet.Count; i++)
				{
					if (openSet[i].FCost < currentField.FCost || openSet[i].FCost == currentField.FCost && openSet[i].hCost < currentField.hCost)
						currentField = openSet[i];
				}

				openSet.Remove(currentField);
				closeSet.Add(currentField);

				if (currentField == targetField)
				{
					RetracePath(startField, targetField);
					return;
				}

				var neighbours = grid.GetNeighbours(currentField);
				foreach(var neighbour in neighbours)
				{
					if (!neighbour.IsAvailable || closeSet.Contains(neighbour))
						continue;

					if (neighbour.IsBlock && neighbour != targetField)
						continue;

					var newMovementCost = currentField.gCost + GetDistance(currentField, neighbour);
					if(newMovementCost < neighbour.gCost || !openSet.Contains(neighbour))
					{
						neighbour.gCost = newMovementCost;
						neighbour.hCost = GetDistance(neighbour, targetField);
						neighbour.parent = currentField;

						if (!openSet.Contains(neighbour))
							openSet.Add(neighbour);
					}
				}
			}
		}

		private void RetracePath(PathField startField, PathField targetField)
		{
			var currentField = targetField;

			while(currentField != startField)
			{
				path.Add(currentField.Position);
				currentField = currentField.parent;
			}

			path.Reverse();
		}

		private int GetDistance(PathField fieldA, PathField fieldB)
		{
			var distanceX = Mathf.Abs(fieldA.GridX - fieldB.GridX);
			var distanceY = Mathf.Abs(fieldA.GridY - fieldB.GridY);

			if (distanceX > distanceY)
				return 14 * distanceY + 10 * (distanceX - distanceY);

			return 14 * distanceX + 10 * (distanceY - distanceX);
		}
	}
}