﻿using Dungeon.Characters.Skills;
using Dungeon.Items;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Dungeon.Settings
{
	[CreateAssetMenu(fileName = "GameplayData", menuName = "Dungeon/Settings/GameplayData", order = 1)]
	public class GameplayData : ScriptableObject
	{
		[Header("Action points costs")]
		[SerializeField] private int moveActionPointsCost;
		[SerializeField] private int useActionPointsCost;
		[SerializeField] private int rotateActionPointsCost;
		[SerializeField] private int useAttributeActionPointsCost;
		[SerializeField] private int useSkillActionPointsCost;
		[SerializeField] private int useItemActionPointsCost;

		[Header("Training points")]
		[SerializeField] private int attributeTrainingPointsForLevel;
		[SerializeField] private int skillTrainingPointsForLevel;
		[SerializeField] private int trainingPointsForSuccess;
		[SerializeField] private int trainingPoinstForCriticalSuccess;
		[SerializeField] private int attributeTrainingPointsForSkillLevelUp;

		[Header("MinMax and modifiers")]
		[SerializeField] private int untrainedSkillModifier;
		[SerializeField] private int maxSkillLevel;
		[SerializeField] private int maxAttributeLevel;

		private List<SkillData> skillsData = null;
		private List<ItemData> itemsData = null;

		public int MoveActionPointsCost => moveActionPointsCost;
		public int UseActionPointsCost => useActionPointsCost;
		public int RotateActionPointsCost => rotateActionPointsCost;
		public int UseAttributeActionPointsCost => useAttributeActionPointsCost;
		public int UseSkillActionPointsCost => useSkillActionPointsCost;
		public int UseItemActionPointsCost => useItemActionPointsCost;

		public int AttributeTrainingPointsForLevel => attributeTrainingPointsForLevel;
		public int SkillTrainingPointsForLevel => skillTrainingPointsForLevel;
		public int TrainingPointsForSuccess => trainingPointsForSuccess;
		public int TrainingPoinstForCriticalSuccess => trainingPoinstForCriticalSuccess;
		public int AttributeTrainingPointsForSkillLevelUp => attributeTrainingPointsForSkillLevelUp;

		public int UntrainedSkillModifier => untrainedSkillModifier;
		public int MaxSkillLevel => maxSkillLevel;
		public int MaxAttributeLevel => maxAttributeLevel;

		public void Init()
		{
			skillsData = new List<SkillData>(Resources.LoadAll<SkillData>("Data"));
			itemsData = new List<ItemData>(Resources.LoadAll<ItemData>("Data"));
		}

		public SkillData GetSkillData(SkillName skillName)
		{
			return skillsData.First(x => x.SkillName == skillName);
		}

		public T GetItemData<T>(int itemId) where T: ItemData
		{
			return itemsData.First(x => x.ItemId == itemId) as T;
		}
	}
}