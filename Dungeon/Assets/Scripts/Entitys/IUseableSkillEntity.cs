﻿using Dungeon.Characters.Skills;

namespace Dungeon.Entitys
{
	public interface IUseableSkillEntity
	{
		void SkillTestResult(SkillName skillType, RollResultType rollResultType, int result);
	}
}