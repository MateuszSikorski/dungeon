﻿using Dungeon.Detectors;
using Dungeon.JobsSystem;
using Dungeon.JobsSystem.Jobs;
using Dungeon.Path;
using Dungeon.UI.Game.Events;
using Dungeon.Utilities.EventBus;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Entitys.EntityBases
{
	public abstract class InteractiveEntity : MonoBehaviour, IClickable, IForJobGeneratorInfo
	{
		protected Job defaultJob;

		public abstract MoveType MoveType { get; }
		public abstract Vector3 PointToRotate { get; }

		public virtual PathField TargetField => GameManager.Instance.PathGrid.GetFieldByPosition(transform.position);

		public void Clicked(ClickedButton button, Vector3 worldClickPosition)
		{
			var gameManager = GameManager.Instance;
			var field = GetField(gameManager, worldClickPosition);

			if (button == ClickedButton.Action)
			{
				gameManager.Player.SetClickedEntity(this, defaultJob, field);
				EventBus.Instance.Fire(new OpenActionMenuEvent(GetAvailableActions(), this));
			}

			if (button == ClickedButton.DefaultAction)
				gameManager.Player.SetClickedEntity(this, defaultJob, field);
		}

		public virtual List<Job> GetAvailableActions()
		{
			var actions = new List<Job>();
			actions.Add(defaultJob);

			return actions;
		}

		protected virtual PathField GetField(GameManager gameManager, Vector3 worldClickPosition)
		{
			return gameManager.PathGrid.GetFieldByPosition(transform.position);
		}
	}
}