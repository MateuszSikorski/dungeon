namespace Dungeon.Entitys
{
	public interface IUseableItemEntity
    {
        int ItemId();
        void UseItem();
    }
}