using Dungeon.Characters;
using Dungeon.Entitys.EntityBases;
using Dungeon.Inventory;
using Dungeon.JobsSystem.Jobs;
using Dungeon.UI.Game.Events;
using Dungeon.Utilities.EventBus;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Entitys.CommonEntitys
{
	public class Chest : InteractiveEntity, IUseableEntity
	{
		[SerializeField] private List<InventoryItem> items;

		private Inventory.Inventory inventory;

		public override MoveType MoveType => MoveType.Close;

		public override Vector3 PointToRotate => transform.position;

		private void Start()
		{
			defaultJob = new UseJob(this);

			inventory = new Inventory.Inventory();
			for (int i = 0; i < items.Count; i++)
				inventory.AddItem(items[i].Data, items[i].Count);

			TargetField.SetIsBlock(true);
		}

		public void Use(CharacterModel character)
		{
			EventBus.Instance.Fire(new OpenExchangeInventoryMenuEvent(character.Inventory, inventory));
		}
	}
}