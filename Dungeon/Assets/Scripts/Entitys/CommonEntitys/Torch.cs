﻿using Dungeon.Characters;
using Dungeon.Entitys.EntityBases;
using Dungeon.JobsSystem.Jobs;
using Dungeon.Magic.Sources;
using UnityEngine;

namespace Dungeon.Entitys.CommonEntitys
{
	public class Torch : InteractiveEntity, IUseableEntity
	{
        [SerializeField] private GameObject lightSource;
        [SerializeField] private MagicSource magicSource;
		[SerializeField] private Transform element;

		public override MoveType MoveType => MoveType.Direct;
		public override Vector3 PointToRotate => element.position;

		private void Start()
		{
			lightSource.SetActive(true);
			magicSource.SetActive(true);

			defaultJob = new UseJob(this);
		}

		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawCube(transform.position, Vector3.one * 0.2f);
		}

		public void Use(CharacterModel character)
		{
			var value = !magicSource.IsActive;
			lightSource.SetActive(value);
			magicSource.SetActive(value);
		}
	}
}