﻿using Dungeon.Characters;
using Dungeon.Characters.Attributes;
using Dungeon.Characters.Skills;
using Dungeon.Entitys.EntityBases;
using Dungeon.Items;
using Dungeon.JobsSystem.Jobs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Entitys.CommonEntitys
{
	public class Door : InteractiveEntity,
		IUseableEntity,
		IUseableAttributeEntity,
		IUseableSkillEntity,
		IUseableItemEntity
	{
		[SerializeField] private Transform door;
		[SerializeField] private bool isLocked;
		[SerializeField] private int breakingModifier;
		[SerializeField] private int lockpickingModifier;
		[SerializeField] private ItemData keyData;

		private bool closed = true;
		private bool wasUsed;

		public override MoveType MoveType => MoveType.Close;
		public override Vector3 PointToRotate => transform.position;

		private void Start()
		{
			defaultJob = new UseJob(this);
			TargetField.SetIsBlock(closed);
		}

		public void Use(CharacterModel character)
		{
			wasUsed = true;

			if (isLocked)
				return;

			StartCoroutine(RotateAnimation());
			closed = !closed;
			TargetField.SetIsBlock(closed);
		}

		public void AttributeTestResult(AttributeType attributeType, RollResultType rollResultType, int result)
		{
			if (attributeType == AttributeType.Strength && RollResult(rollResultType))
				isLocked = false;
		}

		public void SkillTestResult(SkillName skillType, RollResultType rollResultType, int result)
		{
			if (skillType == SkillName.Lockpicking && RollResult(rollResultType))
				isLocked = false;
		}

		public void UseItem()
		{
			isLocked = false;
		}
		public int ItemId()
		{
			return keyData != null ? keyData.ItemId : -1;
		}

		public override List<Job> GetAvailableActions()
		{
			var actions = base.GetAvailableActions();

			if (isLocked && wasUsed)
			{
				actions.Add(new UseAttributeJob(AttributeType.Strength, breakingModifier, this));
				actions.Add(new UseSkillJob(SkillName.Lockpicking, lockpickingModifier, this));
				actions.Add(new UseItemJob(this, true));
			}

			return actions;
		}

		private bool RollResult(RollResultType rollResultType)
		{
			return rollResultType == RollResultType.Success || rollResultType == RollResultType.CriticalSuccess;
		}

		private IEnumerator RotateAnimation()
		{
			var targetValue = 125f;
			if (!closed)
				targetValue = 0f;

			var time = 0f;
			while(time < 1f)
			{
				var angles = door.localEulerAngles;
				angles.y = Mathf.Lerp(angles.y, targetValue, time);
				door.localEulerAngles = angles;

				time += Time.deltaTime / 5f;
				yield return null;
			}
		}
	}
}