﻿using Dungeon.Characters;

namespace Dungeon.Entitys
{
	public interface IUseableEntity
	{
		void Use(CharacterModel character);
	}
}