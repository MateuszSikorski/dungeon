﻿using Dungeon.Characters.Attributes;

namespace Dungeon.Entitys
{
	public interface IUseableAttributeEntity
	{
		void AttributeTestResult(AttributeType attributeType, RollResultType rollResultType, int result);
	}
}