using UnityEngine.Events;
using UnityEngine.UI;

namespace Dungeon.Utilities.Extentions
{
	public static class ButtonExtentions
	{
		public static void SetListener(this Button button, UnityAction listener)
		{
			button.onClick.RemoveAllListeners();
			button.onClick.AddListener(listener);
		}
	}
}