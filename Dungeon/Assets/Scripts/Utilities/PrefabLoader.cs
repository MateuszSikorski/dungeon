﻿using System;
using UnityEngine;

namespace Dungeon.Utilities
{
    public class PrefabLoader : MonoBehaviour
    {
        private const string log = "You try to instantiate anexisting object on path: ";

        public static T InstantiatePrefab<T>(string path, Transform parent = null) where T : UnityEngine.Object
        {
            var prefab = Resources.Load<T>(path);

            if (prefab == null)
                throw new Exception($"{log}{path}");

            var instantiatedObject = Instantiate(prefab, parent);
            var objectOfTypeT = instantiatedObject as T;

            if (objectOfTypeT == null)
                Destroy(instantiatedObject);
            
            return objectOfTypeT;
        }

        public static GameObject InstantiatePrefab(string path, Transform parent = null)
        {
            var prefab = LoadPrefab(path);
            var instantiatedObject = Instantiate(prefab, parent) as GameObject;
            
            return instantiatedObject;
        }

        public static GameObject InstantiatePrefab(string path, Vector3 position, Quaternion rotation)
        {
            var prefab = LoadPrefab(path);
            var instantiatedObject = Instantiate(prefab, position, rotation) as GameObject;

            return instantiatedObject;
        }

        private static UnityEngine.Object LoadPrefab(string path)
		{
            var prefab = Resources.Load(path);

            if (prefab == null)
                throw new Exception($"{log}{path}");

            return prefab;
        }
    }
}