﻿using System;
using System.Collections.Generic;

namespace Dungeon.Utilities
{
	public class List2D<T> where T: class
    {
        private int xSize;
        private int ySize;
        private List<T> list2D;

        public int SizeX => xSize;
        public int SizeY => ySize;

        public List2D(int x, int y)
		{
            xSize = x;
            ySize = y;
            list2D = new List<T>();

            for (int i = 0; i < x * y; i++)
                list2D.Add(null);
		}

        public T this[int x, int y]
		{
            get
			{
                return list2D[GetIndex(x, y)];
            }

            set
			{
                list2D[GetIndex(x, y)] = value;
			}
		}

        private int GetIndex(int x, int y)
		{
            if (x >= xSize || y >= ySize || x < 0 || y < 0)
                throw new ArgumentOutOfRangeException();

            return x * ySize + y;
		}
    }
}