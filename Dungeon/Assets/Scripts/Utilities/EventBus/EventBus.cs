﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Utilities.EventBus
{
	public class EventBus
	{
        private static EventBus instance;

        public static EventBus Instance
        {
            get
            {
                if (instance == null)
                    instance = new EventBus();

                return instance;
            }
        }

        private Dictionary<Type, object> events;

        private EventBus()
        {
            events = new Dictionary<Type, object>();
        }

        public void DeclareEvent<T>() where T : class
        {
            var eventType = typeof(T);
            if (!events.ContainsKey(eventType))
                events.Add(eventType, new Event<T>());
            else
                Debug.LogWarning($"Event of type {eventType.ToString()} already declarated.");
        }

        public void RemoveEvent<T>() where T : class
        {
            var eventType = typeof(T);
            if (events.ContainsKey(eventType))
                events.Remove(eventType);
        }

        public void Subscribe<T>(Action<T> eventAction) where T : class
        {
            CheckEventType<T>("Subscribe");
            (events[typeof(T)] as Event<T>).Subscribe(eventAction);
        }

        public void Unsubscribe<T>(Action<T> eventAction) where T : class
        {
            CheckEventType<T>("Unsubscribe");
            (events[typeof(T)] as Event<T>).Unsubscribe(eventAction);
        }

        public void Fire<T>(T eventData) where T : class
        {
            CheckEventType<T>("Fire");
            (events[typeof(T)] as Event<T>).Fire(eventData);
        }

        private void CheckEventType<T>(string methodName) where T : class
        {
            var type = typeof(T);
            if (!events.ContainsKey(type))
                throw new Exception($"Event of type {type.ToString()} is not declarated! You try to use it in {methodName} method.");
        }
    }
}