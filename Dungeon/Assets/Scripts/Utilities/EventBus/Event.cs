﻿using System;

namespace Dungeon.Utilities.EventBus
{
	public class Event<T> where T: class
	{
        private Action<T> eventAction;

        public void Subscribe(Action<T> newEventAction)
        {
            eventAction += newEventAction;
        }

        public void Unsubscribe(Action<T> eventAction)
        {
            this.eventAction -= eventAction;
        }

        public void Fire(T data)
        {
            eventAction?.Invoke(data);
        }
    }
}