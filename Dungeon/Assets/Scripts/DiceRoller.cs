﻿using UnityEngine;

namespace Dungeon
{
	public enum RollResultType
	{
		CriticalSuccess,
		Success,
		Failure,
		CriticalFailure
	}

	public class DiceRoller
	{
		public static (RollResultType, int) Roll(int difficultyLevel)
		{
			var roll = Random.Range(1, 21);
			var result = difficultyLevel - roll;
			result = result < 0 ? 0 : result;

			var rollResultType = RollResultType.CriticalSuccess;
			if (roll > 1 && roll <= difficultyLevel)
				rollResultType = RollResultType.Success;
			if (roll > difficultyLevel && roll < 20)
				rollResultType = RollResultType.Failure;
			if (roll == 20)
				rollResultType = RollResultType.CriticalFailure;

			Debug.Log($"Difficulti level: {difficultyLevel}, roll: {roll}, result: {result}, roll result type: {rollResultType}");

			return (rollResultType, result);
		}
	}
}