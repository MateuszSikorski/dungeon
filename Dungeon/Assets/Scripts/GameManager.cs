﻿using Dungeon.Characters.Skills;
using Dungeon.Path;
using Dungeon.Settings;
using Dungeon.UI.Game.Events;
using Dungeon.Utilities.EventBus;
using Dungeon.Views.Jobs;
using UnityEngine;

namespace Dungeon
{
	public class GameManager : MonoBehaviour
	{
		[SerializeField] private Player player;
		[SerializeField] private PathGrid pathGrid;
		[SerializeField] private JobsView jobsView;
		[SerializeField] private GameplayData gameplayData;

		private static GameManager instance;

		private GameController gameController = new GameController();

		public static GameManager Instance => instance;

		public GameController GameController => gameController;
		public Player Player => player;
		public PathGrid PathGrid => pathGrid;
		public JobsView JobsView => jobsView;
		public GameplayData GameplayData => gameplayData;

		private void Awake()
		{
			instance = this;

			var eventBus = EventBus.Instance;
			eventBus.DeclareEvent<OpenActionMenuEvent>();
			eventBus.DeclareEvent<OpenExchangeInventoryMenuEvent>();
			eventBus.DeclareEvent<SkillLevelUpEvent>();

			gameplayData.Init();
		}
	}
}