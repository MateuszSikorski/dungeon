﻿namespace Dungeon.Magic.Sources
{
	public class FireSource : MagicSource
	{
		public override SourceType Type => SourceType.Fire;
	}
}