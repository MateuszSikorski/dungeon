﻿using UnityEngine;

namespace Dungeon.Magic.Sources
{
    public enum SourceType
	{
        Fire,
        Wind
	}

    public abstract class MagicSource : MonoBehaviour
    {
        [SerializeField] protected int sourcePower;

        protected bool isActive;

        public int SourcePower => sourcePower;

        public bool IsActive => isActive;

        public abstract SourceType Type { get; }

        public void SetActive(bool value)
		{
            isActive = value;
		}
    }
}