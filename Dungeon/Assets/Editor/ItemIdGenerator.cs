using Dungeon.Items;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Dungeon.Editor
{
	public class ItemIdGenerator
	{
		[MenuItem("Dungeon/Generate Item IDs")]
		public static void GenerateItemId()
		{
			var items = new List<ItemData>(Resources.LoadAll<ItemData>("Data"));

			for(int i = 0; i < items.Count; i++)
			{
				if (items[i].ItemId != 0)
					continue;
				
				items[i].SetId(GenerateId(items));
			}

			AssetDatabase.Refresh();
		}

		private static int GenerateId(List<ItemData> items)
		{
			var newId = 0;
			var idExist = false;

			do
			{
				newId = Random.Range(1, int.MaxValue);
				idExist = items.Find(i => i.ItemId == newId) == null ? false : true;
			}
			while (idExist);

			return newId;
		}
	}
}